<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/Connection.class.php';
	Connection::redirectNoConnect();

	$content['title'] = 'Profile';
	$content['content-title'] = 'Profile de WinXaito';
	$content['content'] = include RACINE.'/views/users/profile.html';

	$content['css'] = '<link rel="stylesheet" href="/intranet/assets/css/project.css"/>';

	require_once RACINE.'/views/default.php';